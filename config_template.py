db = {
    'host': 'localhost',
    'port': 3306,
    'user': 'test',
    'db': 'APOLLOstats',
    'password': 'test',
}
login = {
	'username' : 'ApolloLogin', # Your login information to apollo.rip to access the API
	'password' : 'ApolloPW'
}
options = {
	'updatetime' : 50*60 # The minimum number of seconds between requesting a user's ratio on the website
}
api_path = 'ajax.php'

