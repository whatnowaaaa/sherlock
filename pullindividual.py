from __future__ import division
import pymysql
import requests
import json
import time
import datetime
import xml.etree.ElementTree as etree

from config import db, api_path, login, options

def clamp(n, minn, maxn):
	return max(min(maxn, n), minn)

def loadpage(url,session):
	url = "https://apollo.rip/" + url

	res = session.get(url)
	return res.text

def toBytes(value, type):
	if type == 'B':
		return float(value)
	if type == 'KB':
		return float(value) * 1024
	if type == 'MB':
		return float(value) * 1024 * 1024
	if type == 'GB':
		return float(value) * 1024 * 1024 * 1024
	if type == 'TB':
		return float(value) * 1024 * 1024 * 1024 * 1024

# this reduces accuracy sadly. :( So I disabled it for now
def convertToSignificant(value):
	conversions = 0
	figures = ('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB')
	# while value >= 1024: 
	#     value = value / 1024
	#     conversions = conversions + 1
	return (value, figures[conversions])


# Deletes all data from the site about the user
def removeUser(user, cursor):
	dateQuery = "DELETE FROM usernames WHERE userid=%s;DELETE FROM statistics WHERE username=%s"
	cursor.execute(dateQuery, (user[0], user[2]))
	conn.commit()
	return


def pull_and_store(user, cursor, session):

	# user = [ userid, givenusername, realusername ]
	# should probably fix this by updating the given username in Apollo
	# in fact, why am I having people enter their username when adding? It's not necessary.
	# I should just make it where they put in something like "sherlock=true" to allow to be added.

	# check to make sure the last update for this user wasn't options['updatetime'] seconds ago
	dateQuery = "SELECT date FROM statistics WHERE username=%s ORDER BY date DESC LIMIT 1"
	cursor.execute(dateQuery, (user[1]))
	date = cursor.fetchone()

	if date is not None:
		date = date[0]
		now = datetime.datetime.now()
		deltaTime = now - date
		if deltaTime.seconds < options['updatetime']:
			print("Skipping", user[1], " because they already were updated", deltaTime.seconds, "seconds ago.")
			return

	# download user data
	print("Downloading data for", user[1], user[0])
	data = json.loads(loadpage('%s?action=user&id=%s' % (api_path, user[0]) ,session))


	# ease server load
	time.sleep(5)
	
	# api error handling
	if data['status'] != 'success':
		if data['error'] == 'rate limit exceeded':
			exit("rate limit exceeded. Terminating script")
		if data['status'] == 'failure':
			if data['error'] == 'no such user':
				removeUser(user, cursor)
			print("Error retrieving data for", user[1], user[0], data['error'])
			return
		print("Unknown error retrieving data for", user[1], user[0], data)
		return

	# real username
	user = user + (data['response']["username"] , )
	
	# verify that the username is the same as the one on apollo.rip
	if user[1].lower() != user[2].lower():
		print("Usernames don't match. Deleting all data for this user. ", user[1], "!=", user[2], user[0])
		removeUser(user, cursor)


	# user commands via their profile
	# i.e. sherlock=delete
	profiletext = data['response']['profileText'].lower()
	sherlockCommand = 'sherlock='
	if sherlockCommand+'delete' in profiletext:
		removeUser(user, cursor)
		return
	if sherlockCommand+'ignore' in profiletext:
		return


	# if they don't share their stats then don't add their data
	if data['response']['stats']['downloaded'] is None or data['response']['stats']['uploaded'] is None:
		print("Cannot retrive data for", user[2], user[0], "due to high paranoia")
		return
		
	# fix for 0 B downloaded, for infinite ratio fuckers
	if float(data['response']['stats']['downloaded']) == 0.0:
		ratio = 0
	else:
		ratio = clamp(float(data['response']['stats']['uploaded'])/float(data['response']['stats']['downloaded']),0, 50000)

	stats = data['response']['stats']
	community = data['response']['community']

	downloaded = convertToSignificant(stats['downloaded'])
	uploaded = convertToSignificant(stats['uploaded'])
	buff = convertToSignificant(int(stats['uploaded'])-int(stats['downloaded']))
	
	statslist = [
		user[2], # username
		ratio, # ratio
		downloaded[0], # downloaded
		downloaded[1], # downType
		uploaded[0], # uploaded
		uploaded[1], # upType
		buff[0], # buffer
		buff[1], # buffType
		community['uploaded'], # uploads
		community['snatched'], # snatched
		community['leeching'], # leeching
		community['seeding'], # seeding
		community['posts'] # forumPosts
		]

	# fix None values
	index = 0
	for element in statslist:
		if element is None:
			statslist[index] = 0
		index += 1

	# "yes, I know this is a bad horrible thing to do"
	# quote the original writer of this code. I think it looks fine.
	sqlstr = """INSERT INTO statistics (
		username,
		ratio,
		downloaded,
		downType,
		uploaded,
		upType,
		buffer,
		buffType,
		uploads,
		snatched,
		leeching,
		seeding,
		forumPosts
		)
		VALUES ( %s , %s , %s , %s , %s , %s , %s , %s , %s , %s , %s , %s , %s )""" 

	try:
		cursor.execute(sqlstr, tuple(statslist))
		conn.commit()
	except Exception as e:
		print(str(e))
		print(sqlstr % tuple(statslist))

# -----------------------------------
#        // MAIN PROGRAM \\
# -----------------------------------

# connect to MySQL db
conn = pymysql.connect(host = db['host'],
			port = db['port'],
			user = db['user'],
			passwd = db['password'],
			db = db['db'])
cursor = conn.cursor()

# login to Apollo and store cookie
session = requests.Session()
data = {'username':login['username'], 'password':login['password']}
url = 'https://apollo.rip/login.php'
l = session.post(url, data=data)

# get user ids to lookup and then execute API calls and database insertions
cursor.execute("SELECT userid, username FROM usernames ORDER BY userid ASC")
rows = cursor.fetchall()
for user in rows:
	pull_and_store(user, cursor, session)

cursor.close()
conn.close()
