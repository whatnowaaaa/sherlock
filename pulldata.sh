#!/bin/bash
# this checks whether the script is already running

scriptLocation="/var/www/sherlocko.ga/pullindividual.py"
scriptProcess=$(ps aux | grep "$scriptLocation" | grep -v grep)

if [ -z "$scriptProcess" ] ; then
	 python3 "$scriptLocation"
else
	echo "Error: $scriptLocation is already running" >&2
fi

